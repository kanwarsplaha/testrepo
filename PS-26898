Hi ;rep,

Thank you for contacting Atlassian Support. My understanding is that the side-by-side diff view does not align the highlights (red and green) accurately. I have tried to reproduce the symptom on my local setup and what I noticed is that it happens _only_ if you zoom in and out of the page i.e. the highlights appear misaligned. I can reproduce this on Chrome as well as Firefox (both on Linux). 
Here's a screenshot:

<Link screenshot here>

Let me know if this is what is being observed and does the user see misaligned highlights in side-by-side diff view all the time?  

For further analysis, please generate a [HAR file|https://confluence.atlassian.com/kb/generating-har-files-and-analyzing-web-requests-720420612.html] while reproducing the error and attach it to the ticket. Although this does not appear to be a server-side issue, please also attach a [Support Zip|https://confluence.atlassian.com/support/create-a-support-zip-790796819.html]  so we can corelate client-side requests with any log messages (or errors).

Let me know if you have any questions about the data requested above.

Best Regards,
Kanwar | Atlassian Premier Support
