ssl.noverify=true
http.sslverify=false
http.emptyauth=false
user.name=Kanwar Plaha
user.email=kplaha@atlassian.com
color.status.branch=magenta
color.status.untracked=cyan
color.status.unmerged=yellow
color.status.header=bold black
credential.helper=store --file ~/.my-credentials
filter.lfs.clean=git-lfs clean -- %f
filter.lfs.smudge=git-lfs smudge -- %f
filter.lfs.process=git-lfs filter-process
filter.lfs.required=true
atdc_ad_to_doc_campaign_t
